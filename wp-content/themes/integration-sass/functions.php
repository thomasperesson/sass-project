<?php

function integration_stylesheet()
{
    wp_enqueue_style('style', get_template_directory_uri() . '/assets/css/style.css', array(), false, 'all');
}

add_action('wp_enqueue_scripts', 'integration_stylesheet');
