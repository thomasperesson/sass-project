const mix = require("laravel-mix");
mix.sass("assets/scss/style.scss", "assets/css").options({
    processCssUrls: false,
});

// mix.webpackConfig(
//     {
//         output: {
//             publicPath: '/wp-content/themes/integration-sass/',
//         },
//     }
// );