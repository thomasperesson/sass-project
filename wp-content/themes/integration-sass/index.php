<?php get_header(); ?>

<div class="container">
    <header class="header">
        <div class="upper-block">
            <div class="logo-container">
                <div class="ul-container"><img src="<?php echo get_template_directory_uri() . './assets/img/logo_UL.png'; ?>" alt="Logo UL"></div>
                <div class="insa-container"><img src="<?php echo get_template_directory_uri() . './assets/img/logo_INSA.png'; ?>" alt="Logo INSA"></div>
            </div>
            <div class="upper-right-block">
                <div class="account-container">
                    <a href="#"><i class="far fa-user"></i> My account</a>
                </div>
                <div class="ns-container">
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                    <a href="#"><i class="fab fa-youtube"></i></a>
                    <a href="#"><i class="fab fa-linkedin-in"></i></a>
                </div>
            </div>
        </div>
        <div class="center-block">
            <h1 class="main-title">Trust them</h1>
        </div>
        <div class="left-triangle"></div>
        <div class="right-triangle"></div>
    </header>
    <main class="main">
        <div class="arianne">
            <span>Home</span><span>Trust them</span>
        </div>
        <section class="card-details-section">
            <div class="card-container">
                <div class="card-drop-shadow">
                    <img class="avatar" src="<?php echo get_template_directory_uri() . './assets/img/Ellipse_2.png' ?>" alt="Photo">
                    <div class="card">
                        <div class="card-content">
                            <img class="flag" src="<?php echo get_template_directory_uri() . './assets/img/flag.png' ?>" alt="Flag">
                            <div class="card-group">
                                <span class="label">University :</span>
                                <span class="card-paragraph">Lorem ipsum dolor sit</span>
                            </div>
                            <div class="card-group">
                                <span class="label">Nationality :</span>
                                <span class="card-paragraph">Brasilian</span>
                            </div>
                            <div class="card-group">
                                <span class="label">INSA :</span>
                                <span class="card-paragraph"><span>20/01/2018</span><span>30/04/2018</span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content">
                <h2 class="h2">Erica Romaguera</h2>
                <div class="dpt-context-container">
                    <div class="dpt"><span>Main dpt :</span><span>Biosciences</span></div>
                    <div class="context"><span>Context :</span><span>Exchange programme</span></div>
                </div>
                <h3 class="h3">
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Fuga qui quos consequuntur pariatur adipisci sit! Illum, officia...
                </h3>
                <p class="paragraph">Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem aut voluptatem facere molestiae corporis mollitia aliquam quibusdam cumque consequatur recusandae quos consequuntur, maxime earum quaerat harum, architecto dolores deleniti dolorem?</p>
                <h3 class="h3">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Possimus.
                </h3>
                <p class="paragraph">Lorem ipsum dolor sit amet consectetur adipisicing elit. Non est tenetur rerum provident perspiciatis nesciunt, nisi, error itaque fugit amet at alias atque expedita, distinctio laudantium eius tempore reprehenderit excepturi.</p>
                <h3 class="h3">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nostrum sed sint consectetur quisquam recusandae!</h3>
                <p class="paragraph">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque delectus quisquam, vero animi provident eum, sit, modi aliquam omnis ipsam aut quos obcaecati sapiente? Nam ratione laudantium placeat laborum ducimus!</p>
            </div>
        </section>
        <div class="pagination">
            <div class="pagination-btn"><i class="fas fa-chevron-left"></i></div>
            <div class="pagination-btn"><i class="fas fa-th"></i></div>
            <div class="pagination-btn"><i class="fas fa-chevron-right"></i></div>
        </div>
    </main>
</div>

<?php get_footer(); ?>